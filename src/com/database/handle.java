package com.database;

import java.sql.SQLException;

public class handle {

    public static void errorMessage(String text) {
        System.err.println(text);
    }

    public static void message(String text) {
        System.out.println(text);
    }

    public static void exception(SQLException ex, String function) {
        System.err.printf("SQLException at %s: %s", function, ex.getMessage());
        System.err.printf("SQLException at %s: %s", function, ex.getSQLState());
        System.err.printf("SQLException at %s: %s", function, ex.getErrorCode());
    }
}
