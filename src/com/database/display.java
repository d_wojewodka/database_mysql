package com.database;

import java.sql.*;

public class display {

    public static void showTree(Connection con) {
        try {
            String[] types = {"TABLE"};

            ResultSet rs = con.getMetaData().getTables(null, null, "%", types);

            String catalog = "";
            while (rs.next()) {
                if (!catalog.contains(rs.getString(1))) {
                    System.out.printf("- %s%n", rs.getString(3));
                    catalog = rs.getString(1);
                }

                System.out.printf("   + %s%n", rs.getString(3));
            }
        }
        catch (SQLException ex) {
            handle.exception(ex, "showTree(Connection con)");
        }
    }
}
