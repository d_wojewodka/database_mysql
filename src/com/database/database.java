package com.database;

import java.sql.*;
import java.util.SortedMap;
import java.util.TreeMap;

public class database {
    private static String url = "jdbc:mysql://localhost";
    private static String user = "root";
    private static String password = "123456";

    public static Connection con = null;
    public static PreparedStatement pst = null;
    public static ResultSet rs = null;
    public static ResultSetMetaData rsmd = null;
    public static Statement st = null;
    public static DatabaseMetaData dbmd = null;

    public static String catalog, table = "";

    public database() throws SQLException { // default connection
    }
    public database(String url, String user, String password) throws SQLException {// custom connection
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void connect() {
        System.out.printf("Connecting to DataBase %s as %s...%n", this.url.split("//")[1], this.user);

        try {
            con = DriverManager.getConnection(this.url + "?serverTimezone=UTC", this.user, this.password);
            System.out.println("Successfully connected to DataBase.");
        }
        catch (SQLException ex) {
            handle.exception(ex, "connect()");
        }

        backup.showTree();
    }
    public Connection getConnection() { return this.con; }


    public void selectTable(String catalog, String table) {
        try {
            this.dbmd = con.getMetaData();
            this.rs = this.dbmd.getTables(catalog, null, table, null);

            if (this.rs.next()) {
                this.catalog = catalog;
                this.table = table;
                System.out.printf("Selected %s/%s%n", catalog, table);
            }
            else {
                System.err.printf("Catalog or Table does not exists or table is empty.%n");
                display.showTree(getConnection());
            }
        }
        catch (SQLException ex) {
            handle.exception(ex, "selectTable()");
        }
    }


    static class backup {
        SortedMap<Integer, String[]> table = new TreeMap<Integer, String[]>();
        String catalog = "";

        public static void showTree() {
            try {
                String[] types = {"TABLE"};

                ResultSet rs = con.getMetaData().getTables(null, null, "%", types);

                while (rs.next()) {
                    System.out.println(rs.getString(1));
                    System.out.printf("%s%n", rs.getString(3));
                }
            }
            catch (SQLException ex) {
                handle.exception(ex, "showTree(Connection con)");
            }
        }


    }



    public void disconnect() {
        try {
            con.close();
            System.out.printf("Successfully disconnected from %s", this.url.split("//")[1]);
        }
        catch (SQLException ex) {
            handle.exception(ex, "disconnect()");
        }
    }
}

