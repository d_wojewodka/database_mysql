package com.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class entry {

    public static void addEntry(Object... value) {

    }

    public static void removeEntry(String primary) throws SQLException {
        if (database.con == null) {
            handle.errorMessage("Not connected to database!");
            return;
        }
        if (database.catalog.length() == 0) {
            handle.errorMessage("Catalog is not selected!");
            return;
        }
        if (database.table.length() == 0) {
            handle.errorMessage("Table is not selected!");
            return;
        }

        database.st = database.con.createStatement();
        database.rs = database.st.executeQuery(String.format("SELECT * FROM %s.%s", database.catalog, database.table));
        database.rsmd = database.rs.getMetaData();

        database.st.executeUpdate(String.format("DELETE FROM %s.%s WHERE name = %s", database.catalog, database.table, primary));


    }
}
