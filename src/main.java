import java.util.*;

public class main {
    public static void main(String... args) {
        containers.show();
    }
}


// tutorials for java 8+
// https://docs.oracle.com/javase/8/docs/api/index-files/index-1.html

class containers {
    public static void show() {
        String[] array = new String[5];
        String[][] array2d = new String[3][3];
        String[][][] array3d = new String[2][2][2];
        List<String> list = new ArrayList<String>();
        Set<String> set = new TreeSet<String>();
        Map<Integer, String> map = new HashMap<Integer, String>();
        SortedMap<Integer, String> sortedMap = new TreeMap<Integer, String>();

        array[0] = "0";
        array[1] = "1";
        array[4] = "4";
        //https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html
        System.out.printf("array     --> kolumna%n              %s%n", Arrays.toString(array));

        array2d[0][0] = "0/0";
        array2d[0][1] = "0/1";
        array2d[2][1] = "2/1";
        System.out.printf("array2d   --> kolumna/wiersz%n              %s%n", Arrays.deepToString(array2d));

        array3d[0][0][0] = "0/0/0";
        array3d[1][1][0] = "1/1/0";
        array3d[0][1][0] = "0/1/0";
        System.out.printf("array3d   --> kolumna/wiersz/wysokość%n              %s%n", Arrays.deepToString(array3d));

        list.add("0");
        list.add("1");
        list.add("");
        list.set(2, "2");
        //https://docs.oracle.com/javase/8/docs/api/java/awt/Component.html#list--
        System.out.printf("list      --> indeks%n              %s%n", list.toString());

        set.add("0");
        set.add("1");
        set.add("2");
        //https://docs.oracle.com/javase/8/docs/api/java/util/Set.html
        System.out.printf("set       --> indeks%n              %s%n", set.toString());

        map.put(0, "1");
        map.put(1, "2");
        map.put(5, "3");
        //https://docs.oracle.com/javase/8/docs/api/java/util/Map.html
        System.out.printf("map       --> text%n              %s%n", map.toString());

        sortedMap.put(5, "1");
        sortedMap.put(1, "3");
        sortedMap.put(7, "2");
        //https://docs.oracle.com/javase/8/docs/api/java/util/SortedMap.html
        System.out.printf("sortedMap --> sort%n              %s%n", sortedMap.toString());

    }
}
